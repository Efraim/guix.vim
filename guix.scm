;;; guix.scm -- Guix package definition

(use-modules
  (guix git-download)
  (guix packages)
  (guix gexp)
  (gnu packages vim)
  (srfi srfi-1)
  (ice-9 popen)
  (ice-9 rdelim))

(define %source-dir (dirname (current-filename)))

(define %git-commit
  (read-string (open-pipe "git show HEAD | head -1 | cut -d ' ' -f 2" OPEN_READ)))

(define (keep-file? file stat)
  ;; Return #t if FILE in this repository must be kept, #f otherwise. FILE is
  ;; an absolute file name and STAT is the result of 'lstat' applied to FILE.
  (not (or (any (lambda (str) (string-contains file str))
                '(".git" ".log"))
           (any (lambda (str) (string-suffix? str file))
                '("guix.scm")))))

(define-public vim-guix-vim-git
  (package
    (inherit vim-guix-vim)
    (name "vim-guix-vim")
    (version (git-version (package-version vim-guix-vim) "HEAD" %git-commit))
    (source (local-file %source-dir #:recursive? #t
                      #:select? keep-file?))))

vim-guix-vim-git
