" Vim filetype plugin
" Language:     Guix

if (exists("b:did_ftplugin"))
  finish
endif

let s:cpo = &cpo
set cpo&vim

setlocal lisp
setlocal comments=:;;;;,:;;;,:;;,:;,sr:#\|,mb:\|,ex:\|#
setlocal commentstring=;%s
setlocal define=^\\s*(def\\k*
setlocal iskeyword=33,35-39,42-43,45-58,60-90,94,95,97-122,126

let b:undo_ftplugin = 'setlocal lisp< comments< commentstring< define< iskeyword<'

" Add to the lispwords which occur in files which users of Guix use.
" NB: Vim hardcodes 2 as the indentation. See get_lisp_indent in src/indent.c

setlocal lispwords+=add-after
setlocal lispwords+=add-before
setlocal lispwords+=case
setlocal lispwords+=call-with-input-file
setlocal lispwords+=call-with-input-file
setlocal lispwords+=define
setlocal lispwords+=define*
setlocal lispwords+=define*-public
setlocal lispwords+=define-configuration
setlocal lispwords+=define-deprecated
setlocal lispwords+=define-deprecated/public
setlocal lispwords+=define-deprecated/public-alias
setlocal lispwords+=define-gexp-compiler
setlocal lispwords+=define-module
setlocal lispwords+=define-public
setlocal lispwords+=define-record-type
setlocal lispwords+=define-record-type*
setlocal lispwords+=define-syntax
setlocal lispwords+=define-values
setlocal lispwords+=lambda
setlocal lispwords+=lambda*
setlocal lispwords+=let
setlocal lispwords+=let*
setlocal lispwords+=let*-values
setlocal lispwords+=let-syntax
setlocal lispwords+=let-values
setlocal lispwords+=letrec
setlocal lispwords+=letrec*
setlocal lispwords+=letrec-syntax
setlocal lispwords+=match-lambda
setlocal lispwords+=match-lambda*
setlocal lispwords+=match-record
setlocal lispwords+=mixed-text-file
setlocal lispwords+=modify-inputs
setlocal lispwords+=modify-phases
setlocal lispwords+=modify-services
setlocal lispwords+=parameterize
setlocal lispwords+=plain-file
setlocal lispwords+=program-file
setlocal lispwords+=replace
setlocal lispwords+=set!
setlocal lispwords+=substitute*
setlocal lispwords+=substitute-keyword-arguments
setlocal lispwords+=syntax-rules
setlocal lispwords+=unless
setlocal lispwords+=when
setlocal lispwords+=while
setlocal lispwords+=with-directory-excursion
setlocal lispwords+=with-extensions
setlocal lispwords+=with-fluids
setlocal lispwords+=with-imported-modules
setlocal lispwords+=with-input-to-file
setlocal lispwords+=with-output-to-file
setlocal lispwords+=with-parameters
setlocal lispwords+=wrap-program
setlocal lispwords+=wrap-script

let b:undo_ftplugin = b:undo_ftplugin . ' lispwords<'

compiler guix

let b:did_ftplugin = 1
let &cpo = s:cpo
unlet s:cpo
