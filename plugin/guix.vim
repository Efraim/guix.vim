if exists('g:loaded_guix')
  finish
endif
let g:loaded_guix = 1

command -bar -bang -nargs=+ -complete=custom,ListCmds Guix call guix#disambiguate(<bang>0, <q-args>)

function ListCmds(A, L, P)
    " The exact guix command shouldn't matter
    if stridx(a:L, ' ') == strridx(a:L, ' ')
      return system("guix help | grep '^    ' | cut -f5 -d' '")
    else
      let cmd=split(a:L, ' ')[1]
      return system("guix " . cmd . " --help | cut -c7- | grep ^- | cut -f1 -d' '")
    endif
endfunction
