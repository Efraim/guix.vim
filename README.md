# guix.vim

[![SWH](https://archive.softwareheritage.org/badge/origin/https://git.sr.ht/~efraim/guix.vim)](https://archive.softwareheritage.org/browse/origin/https://git.sr.ht/~efraim/guix.vim)

[![Packaging status](https://repology.org/badge/vertical-allrepos/vim-guix-vim.svg)](https://repology.org/project/vim-guix-vim/versions)

This plugin aims to be usable inside Guix specific files, such as `guix.scm`
or `manifest.scm`. There is some work in progress to extend the syntax
highlighting and indentation rules already applied to scheme files to better
match the Guix coding conventions.

This plugin makes use of the 'CompilerSet' function so that it is possible to
run `:make` from inside vim to build the `guix.scm` file.

This plugin also supports using standard `guix` commands from inside of vim.
This means you can type `:Guix build hello` and the `hello` package will be
built for you.

## Configuration
Current defaults:
   ```viml
   let g:guix_binary = "guix"
   let g:guix_build_options = ""
   ```

If you want to build without grafts then add to your `.vimrc` or
`~/.config/nvim/init.vim`:
  ```viml
  let g:guix_build_options = "--no-grafts"
  ```

## License
This package is released under the same terms as Vim itself.

// set ft=markdown textwidth=80
