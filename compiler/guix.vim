" Vim compiler file
" Compiler:     Build guix.scm file (guix build -f guix.scm)

if exists("current_compiler")
  finish
endif
let current_compiler = "guix"

if !exists("g:guix_binary")
  let g:guix_binary = "guix"
endif

if !exists("g:guix_build_options")
  if exists("$GUIX_BUILD_OPTIONS")
    let g:guix_build_options=$GUIX_BUILD_OPTIONS
  else
    let g:guix_build_options = ""
  endif
endif

if exists(":CompilerSet") != 2
  command -nargs=* CompilerSet setlocal <args>
endif

let s:cpo = &cpo
set cpo&vim

execute 'CompilerSet makeprg='
      \ . escape(g:guix_binary, ' ')
      \ . '\ build\ '
      \ . escape(g:guix_build_options, ' ')
      \ . '\ -f\ %'

CompilerSet errorformat&

let &cpo = s:cpo
unlet s:cpo
