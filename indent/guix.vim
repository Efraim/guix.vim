" Language: GNU Guix

if exists("b:did_indent")
  finish
endif

" Use the Lisp indenting like scheme until we work one out ourselves
runtime! indent/lisp.vim
