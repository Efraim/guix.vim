au BufRead,BufNewFile guix.scm            set filetype=guix
au BufRead,BufNewFile manifest.scm        set filetype=guix
au BufRead,BufNewFile /etc/config.scm     set filetype=guix
