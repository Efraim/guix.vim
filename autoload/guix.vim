if exists('g:autoloaded_guix')
  finish
endif
let g:autoloaded_guix = 1

if !exists('g:guix_binary')
  let g:guix_binary = 'guix'
endif

if !exists('g:guix_build_options')
  if getenv('GUIX_BUILD_OPTIONS') != v:null
    let g:guix_build_options=getenv('GUIX_BUILD_OPTIONS')
  else
    let g:guix_build_options = ""
  endif
endif


function s:parse_commands(text)
  let l:arg_list = split(a:text, ' ')
  let l:parsed_commands = []

  let i = 0
  let l:m_pos = index(l:arg_list, '-m', i)
  while l:m_pos != -1
    let l:complex_pkg = arg_list[l:m_pos:l:m_pos+1]
    let l:parsed_commands += l:arg_list[i:l:m_pos-1] + l:complex_pkg
    let i = l:m_pos + 2
    let l:m_pos = index(l:arg_list, '-m', i)
  endwhile
  if i <= len(l:arg_list)
    let l:parsed_commands += l:arg_list[i:-1]
  endif

  let l:arg_list = l:parsed_commands
  let l:parsed_commands = []
  let i = 0
  let l:e_pos = index(l:arg_list, '-e', i)
  while l:e_pos != -1
    let l:complex_pkg = arg_list[l:e_pos+1:l:e_pos+5]
    " We need to strip the single-quote mark from the expression
    let l:parsed_commands += l:arg_list[i:l:e_pos] + split(join(l:complex_pkg, ' '), "'")
    let i = l:e_pos + 6
    let l:e_pos = index(l:arg_list, '-e', i)
  endwhile
  if i <= len(l:arg_list)
    let l:parsed_commands += l:arg_list[i:-1]
  endif
  return l:parsed_commands
endfunction

function s:start_terminal(command)
  if has('terminal')        " vim specific
    let package_build = term_start(a:command, s:term_options)
  elseif has('nvim')
    let package_build = termopen(a:command, s:term_options)
  else
    " guix_binary breaks with spaces
    " guix_build_options eats the first package
    let l:guix_pos = index(a:command, 'guix')
    let l:args = a:command[l:guix_pos+1:-1]
    execute '!guix ' . join(l:args, ' ')
  endif
endfunction

function guix#disambiguate(bang, text) abort
  let just_pkgpath = ['archive', 'challenge', 'copy', 'edit', 'graph', 'import', 'lint', 'refresh', 'repl', 'search', 'show', 'size', 'style', 'weather']
  let no_opts = ['describe', 'download', 'gc', 'git', 'hash', 'offload', 'processes', 'publish', '--help', '--version']
  let pkgs = s:parse_commands(a:text)
  let s:guixcmd = split(g:guix_binary, ' ')
  let s:guixopts = split(g:guix_build_options, ' ')
  let s:term_options = {
              \ 'term_name': g:guix_binary . a:text,
              \ 'hidden': a:bang,
              \ }
  if index(just_pkgpath, pkgs[0]) >= 0
    call OnlyPkgpath(pkgs)
  elseif index(no_opts, pkgs[0]) >= 0
    call NoBuildOpts(pkgs)
  else
    call FullOptions(pkgs)
  endif
endfunction

function FullOptions(pkgs) abort
  call s:start_terminal(s:guixcmd + [a:pkgs[0]] + s:guixopts + a:pkgs[1:-1])
endfunction

function OnlyPkgpath(pkgs) abort
  let i=0
  while i<len(s:guixopts) - 1
    if s:guixopts[i] == '-L' || '--load-path'
      if getenv('GUIX_PACKAGE_PATH') == v:null
        call setenv('GUIX_PACKAGE_PATH', s:guixopts[i+1])
      else
        call setenv('GUIX_PACKAGE_PATH', getenv('GUIX_PACKAGE_PATH') . ':' . s:guixopts[i+1])
      endif
    endif
    let i += 1
  endwhile
  " Deduplicate
  if getenv('GUIX_PACKAGE_PATH') != v:null
    call setenv('GUIX_PACKAGE_PATH', join(uniq(sort(split(getenv('GUIX_PACKAGE_PATH'), ':'))), ':'))
  endif
  if a:pkgs[0] ==? 'import'
      " We need the guix from $PATH because it is wrapped with guile-semver.
      let full_cmd = ['guix'] + a:pkgs
      let s:term_options['term_name']= 'guix ' . join(a:pkgs, ' ')
  else
      let full_cmd = s:guixcmd + a:pkgs
  endif
  if a:pkgs[0] ==? 'edit'
      call setenv('VISUAL', 'echo')     " see (guix scripts edit)
      silent let mylocation = split(system(join(full_cmd, ' ') . ' 2>/dev/null'), '\s\zs')
      let i=0
      while i<len(mylocation) - 1
          execute('edit' . mylocation[i] . mylocation[i+1])
          let i+=2
      endwhile
      return
  endif
  call s:start_terminal(full_cmd)
endfunction

function NoBuildOpts(pkgs) abort
  call s:start_terminal(s:guixcmd + a:pkgs)
endfunction

