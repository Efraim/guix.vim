" Vim syntax file
" Language: GNU Guix

if exists("b:current_syntax")
  finish
endif

let s:cpo = &cpo
set cpo&vim

" Start with the syntax for scheme and extend it as needed.
runtime! syntax/scheme.vim

" Additions to scheme.vim

" Ideally we'd want to match from '(use-modules' to it's closing ')' but for
" now we match from '(use-modules' to the first '))' and then also a bunch of
" the included modules. This is a workaround to the desired behavior.
" TODO: Don't lose the closing ).
"syn region schemeImport start="\((use-modules[ \t\n]*\)" end="))" "contains=ALLBUT,schemeComment
syn region schemeImport start="\((use-modules[ \t\n]*\)" end=")"
syn region schemeImport start="\((define-module[ \t\n]*\)" end=")"
" This doesn't match keywords not listed.
syn region schemeImport start="(\?(\(guix\|gnu\|srfi\|ice-9\)[ )]" skip=")[ \t\n]*#:"  end=")" "contained

syn keyword schemeSyntax define-public
syn keyword schemeSyntax define\*-public
syn keyword schemeSyntax define-public\*

" Special for guix
syn keyword packageKeyword arguments
syn keyword packageKeyword build-system
" syn keyword sourceKeyword commit
syn keyword packageKeyword description
syn keyword packageKeyword home-page
syn keyword packageKeyword inherit
" syn keyword packageKeyword inputs
syn keyword packageKeyword license
" syn keyword sourceKeyword method
" syn keyword packageKeyword name
syn keyword packageKeyword native-inputs
syn keyword packageKeyword native-search-paths
syn keyword packageKeyword origin
" syn keyword packageKeyword outputs
syn keyword packageKeyword package
syn keyword packageKeyword propagated-inputs
syn keyword packageKeyword properties
syn keyword packageKeyword search-path-specification
syn keyword packageKeyword search-paths
syn keyword packageKeyword source
syn keyword packageKeyword supported-systems
syn keyword packageKeyword synopsis
" syn keyword sourceKeyword uri
" syn keyword sourceKeyword url
" syn keyword packageKeyword version

" syn keyword sourceKeyword base32
syn keyword sourceKeyword file-name
syn keyword sourceKeyword modules
" syn keyword sourceKeyword sha256
syn keyword sourceKeyword snippet

" syn keyword package package

"syn region package start="(package[ \t\n]" end=")" contains=packageKeyword,sourceKeyword
"syn region homepage start="(home-page" end=")" contains=schemeString,@NoSpell
"syn region synopsis start="(synopsis" end=")" contains=schemeString,@Spell
"syn region description start="(description" end=")" contains=schemeString,@Spell

" syn keyword packageArgumentFields #:configure-flags
" syn keyword packageArgumentFields #:make-flags
" syn keyword packageArgumentFields #:tests?
" syn keyword packageArgumentFields #:test-target
" syn keyword packageArgumentFields #:parallel-tests?
" syn keyword packageArgumentFields #:modules
" syn keyword packageArgumentFields #:python
" syn keyword packageArgumentFields #:use-setuptools?
" syn keyword packageArgumentFields #:phases
" syn keyword packageArgumentFields #:select
" syn keyword packageArgumentFields (modify-phases
" syn keyword packageArgumentFields (add-after
" syn keyword packageArgumentFields (add-before
" syn keyword packageArgumentFields (replace
" syn keyword packageArgumentFields (delete
" syn keyword packageArgumentFields ,@(substitute-keyword-arguments
" syn keyword packageArguments arguments

" syn region packageArguments start="`(" end=")" contains=packageArgumentFields
" syn region packageArguments start="'(" end=")" contains=packageArgumentFields

" No highlighting until we can figure out something useful
" hi def link packageKeyword Function
" hi def link sourceKeyword Function
" hi def link packageArgumentFields Function

let b:current_syntax = 'guix'
let &cpo = s:cpo
unlet s:cpo
